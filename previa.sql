-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: 15-Jan-2016 às 15:48
-- Versão do servidor: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mariabrasil`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 0, '20160115143655_home1.jpg', '2016-01-15 16:36:56', '2016-01-15 16:36:56'),
(2, 0, '20160115143700_home2.jpg', '2016-01-15 16:37:01', '2016-01-15 16:37:01'),
(3, 0, '20160115143704_home3.jpg', '2016-01-15 16:37:05', '2016-01-15 16:37:05'),
(4, 0, '20160115143709_home4.jpg', '2016-01-15 16:37:09', '2016-01-15 16:37:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clipping`
--

CREATE TABLE `clipping` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `clipping`
--

INSERT INTO `clipping` (`id`, `ordem`, `imagem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 0, '20160115144216_projeto2.png', 'Exemplo', 'exemplo', '2016-01-15 16:42:17', '2016-01-15 16:42:17');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clipping_imagens`
--

CREATE TABLE `clipping_imagens` (
`id` int(10) unsigned NOT NULL,
  `clipping_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `clipping_imagens`
--

INSERT INTO `clipping_imagens` (`id`, `clipping_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '20160115144227_projeto3.png', '2016-01-15 16:42:28', '2016-01-15 16:42:28'),
(2, 1, 0, '20160115144227_projeto2.png', '2016-01-15 16:42:28', '2016-01-15 16:42:28');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
`id` int(10) unsigned NOT NULL,
  `telefones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pinterest` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rss` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `telefones`, `email`, `endereco`, `googlemaps`, `facebook`, `pinterest`, `rss`, `instagram`, `created_at`, `updated_at`) VALUES
(1, '11 2659 8056, 11 2659 8057, 11 2659 8058', 'contato@mariabrasilarquitetura.com.br', '<p>Av. Brigadeiro Faria Lima, 2013</p>\r\n\r\n<p>Conj. 5e - 7 - Pinheiros</p>\r\n\r\n<p>S&atilde;o Paulo - SP - CEP: 01452-001</p>\r\n', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d117016.47131695956!2d-46.63300512050714!3d-23.576891879574223!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5772eee33c85%3A0x1f162bd68723b753!2sAv.+Brg.+Faria+Lima%2C+2013+-+Jardim+Paulistano%2C+S%C3%A3o+Paulo+-+SP%2C+01452-001!5e0!3m2!1spt-BR!2sbr!4v1452616579156" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '/', '/', '/', '/', '0000-00-00 00:00:00', '2016-01-15 16:47:54');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
`id` int(10) unsigned NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `assunto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dicas`
--

CREATE TABLE `dicas` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `dicas`
--

INSERT INTO `dicas` (`id`, `ordem`, `titulo`, `slug`, `chamada`, `imagem`, `texto`, `created_at`, `updated_at`) VALUES
(1, 0, 'Exemplo', 'exemplo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', '20160115144156_home3.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et lorem diam. Quisque in scelerisque sapien. Suspendisse commodo pellentesque nisl in pellentesque. Donec maximus arcu quam, quis sollicitudin sapien congue eget. Duis et orci non augue eleifend finibus id sed sapien. Morbi vulputate egestas dignissim. Praesent sit amet finibus ante, a eleifend risus.</p>\r\n', '2016-01-15 16:41:57', '2016-01-15 16:41:57');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_01_12_141559_create_banners_table', 1),
('2016_01_12_161003_create_perfil_table', 1),
('2016_01_12_162529_create_contato_table', 1),
('2016_01_12_162536_create_contatos_recebidos_table', 1),
('2016_01_12_164224_create_dicas_table', 1),
('2016_01_12_212846_create_projetos_tables', 1),
('2016_01_12_224729_create_clipping_tables', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfil`
--

CREATE TABLE `perfil` (
`id` int(10) unsigned NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `perfil`
--

INSERT INTO `perfil` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, '<p>Maria Paula e Maria Claudia Brasil se formaram na Faculdade Belas Artes de S&atilde;o Paulo, no ano de 1996 e 1998, atuando juntas desde ent&atilde;o no mercado de arquitetura e decora&ccedil;&atilde;o.Ao longo destes anos, j&aacute; executaram cerca de 150 projetos - todos com o objetivo principal de aliar ao programa solicitado pelo cliente os quesitos funcionalidade, est&eacute;tica, conforto e qualidade.</p>\r\n\r\n<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et lorem diam.</h3>\r\n\r\n<p>Em um projeto, as arquitetas utilizam sempre que poss&iacute;vel materiais naturais e novas tecnologias, al&eacute;m da valorizac&atilde;o da luz natural. H&aacute; sempre, ainda, a preocupa&ccedil;&atilde;o constante na sele&ccedil;&atilde;o de fornecedores que agreguem profissionalismo &agrave;s obras (prazos cumpridos e qualidade na execu&ccedil;&atilde;o). Buscam atrav&eacute;s da conversa que o cliente entenda o que quer para que o resultado final traga-lhe bem-estar e conforto, transformando cada projeto em algo &uacute;nico.</p>\r\n', '20160115143913_foto.png', '0000-00-00 00:00:00', '2016-01-15 16:47:20');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos`
--

CREATE TABLE `projetos` (
`id` int(10) unsigned NOT NULL,
  `projetos_categoria_id` int(10) unsigned DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `projetos`
--

INSERT INTO `projetos` (`id`, `projetos_categoria_id`, `ordem`, `imagem`, `titulo`, `slug`, `texto`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '20160115144126_home1.jpg', 'Exemplo', 'exemplo', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et lorem diam. Quisque in scelerisque sapien. Suspendisse commodo pellentesque nisl in pellentesque. Donec maximus arcu quam, quis sollicitudin sapien congue eget. Duis et orci non augue eleifend finibus id sed sapien. Morbi vulputate egestas dignissim. Praesent sit amet finibus ante, a eleifend risus.</p>\r\n', '2016-01-15 16:40:42', '2016-01-15 16:41:27');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos_categorias`
--

CREATE TABLE `projetos_categorias` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `projetos_categorias`
--

INSERT INTO `projetos_categorias` (`id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 0, 'Residencial', 'residencial', '2016-01-15 16:39:30', '2016-01-15 16:39:30'),
(2, 1, 'Comercial, & Corporativo', 'comercial-corporativo', '2016-01-15 16:39:39', '2016-01-15 16:39:39'),
(3, 2, 'Mostras, & Vitrines', 'mostras-vitrines', '2016-01-15 16:39:56', '2016-01-15 16:39:56');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos_imagens`
--

CREATE TABLE `projetos_imagens` (
`id` int(10) unsigned NOT NULL,
  `projeto_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `projetos_imagens`
--

INSERT INTO `projetos_imagens` (`id`, `projeto_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '20160115144048_projeto2.png', '2016-01-15 16:40:49', '2016-01-15 16:40:49'),
(2, 1, 1, '20160115144052_projeto3.png', '2016-01-15 16:40:53', '2016-01-15 16:40:53');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$ovDpzeozRwlWH25/IUweH.e.YAlAybPbzGuDd/YcBD0qNeHjqNCn.', 'HDYbOvGS4bQqJvjGRyruOD8v91yAD7gl0WxijflNjxqHnTq5NFoRsm7BcrC2', '0000-00-00 00:00:00', '2016-01-15 16:36:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clipping`
--
ALTER TABLE `clipping`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clipping_imagens`
--
ALTER TABLE `clipping_imagens`
 ADD PRIMARY KEY (`id`), ADD KEY `clipping_imagens_clipping_id_foreign` (`clipping_id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dicas`
--
ALTER TABLE `dicas`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `perfil`
--
ALTER TABLE `perfil`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projetos`
--
ALTER TABLE `projetos`
 ADD PRIMARY KEY (`id`), ADD KEY `projetos_projetos_categoria_id_foreign` (`projetos_categoria_id`);

--
-- Indexes for table `projetos_categorias`
--
ALTER TABLE `projetos_categorias`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
 ADD PRIMARY KEY (`id`), ADD KEY `projetos_imagens_projeto_id_foreign` (`projeto_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `clipping`
--
ALTER TABLE `clipping`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `clipping_imagens`
--
ALTER TABLE `clipping_imagens`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dicas`
--
ALTER TABLE `dicas`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `perfil`
--
ALTER TABLE `perfil`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `projetos`
--
ALTER TABLE `projetos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `projetos_categorias`
--
ALTER TABLE `projetos_categorias`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `clipping_imagens`
--
ALTER TABLE `clipping_imagens`
ADD CONSTRAINT `clipping_imagens_clipping_id_foreign` FOREIGN KEY (`clipping_id`) REFERENCES `clipping` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `projetos`
--
ALTER TABLE `projetos`
ADD CONSTRAINT `projetos_projetos_categoria_id_foreign` FOREIGN KEY (`projetos_categoria_id`) REFERENCES `projetos_categorias` (`id`) ON DELETE SET NULL;

--
-- Limitadores para a tabela `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
ADD CONSTRAINT `projetos_imagens_projeto_id_foreign` FOREIGN KEY (`projeto_id`) REFERENCES `projetos` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
