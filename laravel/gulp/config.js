exports.development = {
    vhost  : 'mariabrasil.dev',
    stylus : './resources/assets/stylus/',
    js     : './resources/assets/js/',
    img    : './resources/assets/img/',
    vendor : './resources/assets/vendor/'
};

exports.build = {
    css : '../public/assets/css/',
    js  : '../public/assets/js/',
    img : '../public/assets/img/layout/'
};

exports.vendorPlugins = [
    this.development.vendor + 'jquery-cycle2/build/jquery.cycle2.min.js',
];
