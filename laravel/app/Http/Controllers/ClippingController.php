<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Clipping;

class ClippingController extends Controller
{
    public function index(Clipping $clipping)
    {
        if (! $clipping->exists) {
            $clipping = Clipping::ordenados()->get();

            return view('frontend.clipping.index', compact('clipping'));
        }

        return view('frontend.clipping.show', compact('clipping'));
    }
}
