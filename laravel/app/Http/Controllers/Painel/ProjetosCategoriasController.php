<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProjetoCategoriaRequest;
use App\Http\Controllers\Controller;

use App\Models\ProjetoCategoria;

class ProjetosCategoriasController extends Controller
{
    public function index()
    {
        $categorias = ProjetoCategoria::ordenados()->get();

        return view('painel.projetos.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.projetos.categorias.create');
    }

    public function store(ProjetoCategoriaRequest $request)
    {
        try {

            $input = $request->all();

            ProjetoCategoria::create($input);
            return redirect()->route('painel.projetos.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(ProjetoCategoria $categoria)
    {
        return view('painel.projetos.categorias.edit', compact('categoria'));
    }

    public function update(ProjetoCategoriaRequest $request, ProjetoCategoria $categoria)
    {
        try {

            $input = $request->all();

            $categoria->update($input);
            return redirect()->route('painel.projetos.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(ProjetoCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.projetos.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
