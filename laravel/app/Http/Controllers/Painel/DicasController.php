<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DicaRequest;
use App\Http\Controllers\Controller;

use App\Models\Dica;
use App\Helpers\CropImage;

class DicasController extends Controller
{
    private $image_config = [
        [
            'width'  => 400,
            'height' => 180,
            'path'   => 'assets/img/dicas/thumbs/'
        ],
        [
            'width'  => 740,
            'height' => 500,
            'path'   => 'assets/img/dicas/'
        ]
    ];

    public function index()
    {
        $dicas = Dica::ordenados()->get();

        return view('painel.dicas.index', compact('dicas'));
    }

    public function create()
    {
        return view('painel.dicas.create');
    }

    public function store(DicaRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Dica::create($input);
            return redirect()->route('painel.dicas.index')->with('success', 'Dica adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar dica: '.$e->getMessage()]);

        }
    }

    public function edit(Dica $dica)
    {
        return view('painel.dicas.edit', compact('dica'));
    }

    public function update(DicaRequest $request, Dica $dica)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $dica->update($input);
            return redirect()->route('painel.dicas.index')->with('success', 'Dica alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar dica: '.$e->getMessage()]);

        }
    }

    public function destroy(Dica $dica)
    {
        try {

            $dica->delete();
            return redirect()->route('painel.dicas.index')->with('success', 'Dica excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir dica: '.$e->getMessage()]);

        }
    }
}
