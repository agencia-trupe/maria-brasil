<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClippingRequest;
use App\Http\Controllers\Controller;

use App\Models\Clipping;
use App\Helpers\CropImage;

class ClippingController extends Controller
{
    private $image_config = [
        [
            'width'   => 195,
            'height'  => 270,
            'path'    => 'assets/img/clipping/thumbs/'
        ],
        [
            'width'   => 945,
            'height'  => 800,
            'limite'  => true,
            'path'    => 'assets/img/clipping/'
        ]
    ];

    public function index()
    {
        $clipping = Clipping::ordenados()->get();

        return view('painel.clipping.index', compact('clipping'));
    }

    public function create()
    {
        return view('painel.clipping.create');
    }

    public function store(ClippingRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Clipping::create($input);
            return redirect()->route('painel.clipping.index')->with('success', 'Clipping adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar clipping: '.$e->getMessage()]);

        }
    }

    public function edit(Clipping $clipping)
    {
        return view('painel.clipping.edit', compact('categorias', 'clipping'));
    }

    public function update(ClippingRequest $request, Clipping $clipping)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $clipping->update($input);
            return redirect()->route('painel.clipping.index')->with('success', 'Clipping alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar clipping: '.$e->getMessage()]);

        }
    }

    public function destroy(Clipping $clipping)
    {
        try {

            $clipping->delete();
            return redirect()->route('painel.clipping.index')->with('success', 'Clipping excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir clipping: '.$e->getMessage()]);

        }
    }
}
