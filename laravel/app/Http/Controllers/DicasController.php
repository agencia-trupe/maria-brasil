<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Dica;

class DicasController extends Controller
{
    public function index(Dica $dica)
    {
        if (! $dica->exists) {
            $dicas = Dica::ordenados()->get();

            return view('frontend.dicas.index', compact('dicas'));
        }

        return view('frontend.dicas.show', compact('dica'));
    }
}
