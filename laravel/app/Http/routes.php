<?php

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('perfil', ['as' => 'perfil', 'uses' => 'PerfilController@index']);
Route::get('clipping/{clipping_slug?}', ['as' => 'clipping', 'uses' => 'ClippingController@index']);
Route::get('dicas/{dicas_slug?}', ['as' => 'dicas', 'uses' => 'DicasController@index']);
Route::get('projetos/{projetos_categoria_slug?}', ['as' => 'projetos.index', 'uses' => 'ProjetosController@index']);
Route::get('projetos/{projetos_categoria_slug}/{projeto_slug}', ['as' => 'projetos.show', 'uses' => 'ProjetosController@show']);
Route::get('contato', ['as' => 'contato', 'uses' => 'ContatoController@index']);
Route::post('contato', ['as' => 'contato.envio', 'uses' => 'ContatoController@envio']);

// Painel
Route::group(['prefix' => 'painel', 'namespace' => 'Painel', 'middleware' => 'auth'], function() {
    Route::get('/', ['as' => 'painel', 'uses' => 'HomeController@index']);
    Route::resource('banners', 'BannersController');
    Route::resource('perfil', 'PerfilController');
    Route::resource('projetos/categorias', 'ProjetosCategoriasController');
    Route::resource('projetos', 'ProjetosController');
    Route::resource('projetos.imagens', 'ProjetosImagensController');
    Route::resource('dicas', 'DicasController');
    Route::resource('clipping', 'ClippingController');
    Route::resource('clipping.imagens', 'ClippingImagensController');
    Route::resource('contato/recebidos', 'ContatosRecebidosController');
    Route::resource('contato', 'ContatoController');
    Route::resource('usuarios', 'UsuariosController');
    Route::post('order', 'HomeController@order');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});


// Auth
Route::group(['prefix' => 'painel', 'namespace' => 'Auth'], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'auth', 'uses' => 'AuthController@postLogin']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
});
