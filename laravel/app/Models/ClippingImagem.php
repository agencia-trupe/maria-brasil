<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClippingImagem extends Model
{
    protected $table = 'clipping_imagens';

    protected $guarded = ['id'];

    public function scopeClipping($query, $id)
    {
        return $query->where('clipping_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
