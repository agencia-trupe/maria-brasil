<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjetoImagem extends Model
{
    protected $table = 'projetos_imagens';

    protected $guarded = ['id'];

    public function scopeProjeto($query, $id)
    {
        return $query->where('projeto_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
