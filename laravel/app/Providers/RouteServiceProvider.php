<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('banners', 'App\Models\Banner');
        $router->model('perfil', 'App\Models\Perfil');
        $router->model('categorias', 'App\Models\ProjetoCategoria');
        $router->model('projetos', 'App\Models\Projeto');
        $router->model('dicas', 'App\Models\Dica');
        $router->model('clipping', 'App\Models\Clipping');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('imagens', function($id, $route, $model = null) {
            if ($route->hasParameter('projetos')) {
                $model = \App\Models\ProjetoImagem::find($id);
            } elseif ($route->hasParameter('clipping')) {
                $model = \App\Models\ClippingImagem::find($id);
            }

            return $model ?: \App::abort('404');
        });

        $router->bind('projetos_categoria_slug', function($value) {
            return \App\Models\ProjetoCategoria::with('projetos')->slug($value)->first() ?: \App::abort('404');
        });
        $router->bind('projeto_slug', function($value) {
            return \App\Models\Projeto::with('imagens')->slug($value)->first() ?: \App::abort('404');
        });

        $router->bind('dicas_slug', function($value) {
            return \App\Models\Dica::slug($value)->first() ?: \App::abort('404');
        });

        $router->bind('clipping_slug', function($value) {
            return \App\Models\Clipping::with('imagens')->slug($value)->first() ?: \App::abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
