<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */

    "accepted"         => "O campo <strong>:attribute</strong> deve ser aceito.",
    "active_url"       => "O campo <strong>:attribute</strong> não contém um URL válido.",
    "after"            => "O campo <strong>:attribute</strong> deverá conter uma data posterior a :date.",
    "alpha"            => "O campo <strong>:attribute</strong> deverá conter apenas letras.",
    "alpha_dash"       => "O campo <strong>:attribute</strong> deverá conter apenas letras, números e traços.",
    "alpha_num"        => "O campo <strong>:attribute</strong> deverá conter apenas letras e números .",
    "array"            => "O campo <strong>:attribute</strong> precisa ser um conjunto.",
    "before"           => "O campo <strong>:attribute</strong> deverá conter uma data anterior a :date.",
    "between"          => [
        "numeric" => "O campo <strong>:attribute</strong> deverá ter um valor entre :min - :max.",
        "file"    => "O campo <strong>:attribute</strong> deverá ter um tamanho entre :min - :max kilobytes.",
        "string"  => "O campo <strong>:attribute</strong> deverá conter entre :min - :max caracteres.",
        "array"   => "O campo <strong>:attribute</strong> precisar ter entre :min - :max itens.",
    ],
    "boolean"          => "O campo <strong>:attribute</strong> deverá ter o valor verdadeiro ou falso.",
    "confirmed"        => "A confirmação para o campo <strong>:attribute</strong> não coincide.",
    "date"             => "O campo <strong>:attribute</strong> não contém uma data válida.",
    "date_format"      => "A data indicada para o campo <strong>:attribute</strong> não respeita o formato :format.",
    "different"        => "Os campos <strong>:attribute</strong> e :other deverão conter valores diferentes.",
    "digits"           => "O campo <strong>:attribute</strong> deverá conter :digits dígitos.",
    "digits_between"   => "O campo <strong>:attribute</strong> deverá conter entre :min a :max dígitos.",
    "email"            => "O campo <strong>:attribute</strong> não contém um endereço de email válido.",
    "exists"           => "O valor selecionado para o campo <strong>:attribute</strong> é inválido.",
    "filled"           => "É obrigatória a indicação de um valor para o campo <strong>:attribute</strong>.",
    "image"            => "O campo <strong>:attribute</strong> deverá conter uma imagem.",
    "in"               => "O campo <strong>:attribute</strong> não contém um valor válido.",
    "integer"          => "O campo <strong>:attribute</strong> deverá conter um número inteiro.",
    "ip"               => "O campo <strong>:attribute</strong> deverá conter um IP válido.",
    "max"              => [
        "numeric" => "O campo <strong>:attribute</strong> não deverá conter um valor superior a :max.",
        "file"    => "O campo <strong>:attribute</strong> não deverá ter um tamanho superior a :max kilobytes.",
        "string"  => "O campo <strong>:attribute</strong> não deverá conter mais de :max caracteres.",
        "array"   => "O campo <strong>:attribute</strong> deve ter no máximo :max itens.",
    ],
    "mimes"            => "O campo <strong>:attribute</strong> deverá conter um arquivo do tipo: :values.",
    "min"              => [
        "numeric" => "O campo <strong>:attribute</strong> deverá ter um valor superior ou igual a :min.",
        "file"    => "O campo <strong>:attribute</strong> deverá ter no mínimo :min kilobytes.",
        "string"  => "O campo <strong>:attribute</strong> deverá conter no mínimo :min caracteres.",
        "array"   => "O campo <strong>:attribute</strong> deve ter no mínimo :min itens.",
    ],
    "not_in"           => "O campo <strong>:attribute</strong> contém um valor inválido.",
    "numeric"          => "O campo <strong>:attribute</strong> deverá conter um valor numérico.",
    "regex"            => "O formato do valor para o campo <strong>:attribute</strong> é inválido.",
    "required"         => "O campo <strong>:attribute</strong> é obrigatório.",
    "required_if"      => "É obrigatória a indicação de um valor para o campo <strong>:attribute</strong> quando o valor do campo :other é igual a :value.",
    "required_with"    => "É obrigatória a indicação de um valor para o campo <strong>:attribute</strong> quando :values está presente.",
    "required_with_all" => "É obrigatória a indicação de um valor para o campo <strong>:attribute</strong> quando um dos :values está presente.",
    "required_without" => "É obrigatória a indicação de um valor para o campo <strong>:attribute</strong> quanto :values não está presente.",
    "required_without_all" => "É obrigatória a indicação de um valor para o campo <strong>:attribute</strong> quando nenhum dos :values está presente.",
    "same"             => "Os campos <strong>:attribute</strong> e :other deverão conter valores iguais.",
    "size"             => [
        "numeric" => "O campo <strong>:attribute</strong> deverá conter o valor :size.",
        "file"    => "O campo <strong>:attribute</strong> deverá ter o tamanho de :size kilobytes.",
        "string"  => "O campo <strong>:attribute</strong> deverá conter :size caracteres.",
        "array"   => "O campo <strong>:attribute</strong> deve ter :size itens.",
    ],
    "string"           => "O campo <strong>:attribute</strong> deve ser uma string.",
    "timezone"         => "O campo <strong>:attribute</strong> deverá ter um fuso horário válido.",
    "unique"           => "O valor indicado para o campo <strong>:attribute</strong> já se encontra utilizado.",
    "url"              => "O formato do URL indicado para o campo <strong>:attribute</strong> é inválido.",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'name' => 'usuário',
        'email' => 'e-mail',
        'password' => 'senha',
        'endereco' => 'endereço',
        'googlemaps' => 'código googlemaps',
        'titulo' => 'título'
    ],

];
