<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('site.name') }} - Painel Administrativo</title>

    <link rel="stylesheet" href="{{ asset('assets/painel/css/bootstrap.min.css') }}">
    <script>var BASE = '{{ url() }}'</script>

    <style>
        body {
            background: -webkit-linear-gradient(90deg, #185a9d 10%, #43cea2 120%);
            background: -moz-linear-gradient(90deg, #185a9d 10%, #43cea2 120%);
            background: -ms-linear-gradient(90deg, #185a9d 10%, #43cea2 120%);
            background: -o-linear-gradient(90deg, #185a9d 10%, #43cea2 120%);
            background: linear-gradient(90deg, #185a9d 10%, #43cea2 120%);
        }
        .login {
            padding: 30px;
            background: #fff;
            border-radius: 5px;
            margin: 100px auto 0;
            float: none;
            -webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
            -moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
            box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
        }
        .input-group {
            margin-bottom: 5px;
        }
        h3 {
            margin: 0 0 30px;
        }
        h3 small {
            display: block;
            margin-top: 10px;
        }
        @media(max-width: 768px) {
            .login {
                margin-top: 30px;
            }
        }
    </style>
</head>
<body>
    <div class="col-md-4 col-sm-6 col-xs-10 login">
    {!! Form::open(['route' => 'auth']) !!}
        <h3>
            {{ config('site.name') }}
            <small>Painel Administrativo</small>
        </h3>

        @if(session('error'))
        <div class="alert alert-danger">{{ session('error') }}</div>
        @endif

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                {!! Form::text('login', null, [
                    'class'       => 'form-control',
                    'placeholder' => 'usuário ou e-mail',
                    'autofocus'   => '',
                    'required'    => ''
                ]) !!}
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                {!! Form::password('password', [
                    'class'       => 'form-control',
                    'placeholder' => 'senha',
                    'required'    => ''
                ]) !!}
            </div>
        </div>

        <div class="checkbox">
            <label>
                {!! Form::checkbox('remember', 1, null, ['id' => 'remember']) !!}
                <small>Lembrar de mim</small>
            </label>
        </div>

        {!! Form::submit('Login', ['class' => 'btn btn-success btn-block', 'style' => 'margin-top:20px;']) !!}
    {!! Form::close() !!}
    </div>

    <script src=""></script>
</body>
</html>
