@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Dicas /</small> Editar Dica</h2>
    </legend>

    {!! Form::model($dica, [
        'route'  => ['painel.dicas.update', $dica->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.dicas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
