@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Dicas /</small> Adicionar Dica</h2>
    </legend>

    {!! Form::open(['route' => 'painel.dicas.store', 'files' => true]) !!}

        @include('painel.dicas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
