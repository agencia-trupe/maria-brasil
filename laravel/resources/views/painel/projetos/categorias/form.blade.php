@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título (use a vírgula para inserir uma quebra de linha)') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.projetos.categorias.index') }}" class="btn btn-default btn-voltar">Voltar</a>
