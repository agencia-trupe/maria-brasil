@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('telefones', 'Telefones (separados por vírgula)') !!}
    {!! Form::text('telefones', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('endereco', 'Endereço') !!}
    {!! Form::textarea('endereco', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
</div>

<div class="form-group">
    {!! Form::label('googlemaps', 'Código GoogleMaps') !!}
    {!! Form::text('googlemaps', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('facebook', 'Facebook (opcional)') !!}
    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('pinterest', 'Pinterest (opcional)') !!}
    {!! Form::text('pinterest', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('rss', 'RSS Feed (opcional)') !!}
    {!! Form::text('rss', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('instagram', 'Instagram (opcional)') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
