@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Perfil</h2>
    </legend>

    {!! Form::model($perfil, [
        'route'  => ['painel.perfil.update', $perfil->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.perfil.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection