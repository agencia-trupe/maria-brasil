@extends('frontend.common.template')

@section('content')

    <div class="banner-home">
        <div class="banner-fotos">
            <div class="banner-fotos-wrapper banner-fotos-wrapper-1">
                <div class="banner-fotos banner-fotos-1"></div>
            </div>
            <div class="banner-fotos-wrapper banner-fotos-wrapper-2">
                <div class="banner-fotos banner-fotos-2"></div>
            </div>
            <div class="banner-fotos-wrapper banner-fotos-wrapper-3">
                <div class="banner-fotos banner-fotos-3"></div>
            </div>
            <div class="banner-fotos-wrapper banner-fotos-wrapper-4">
                <div class="banner-fotos banner-fotos-4"></div>
            </div>
        </div>

        <div class="overlay"></div>

        <div class="center">
            <nav>
                @include('frontend.common.nav')
            </nav>

            <div class="logo">
                <h1>{{ config('site.name') }}</h1>
            </div>
        </div>
    </div>

@endsection
