    <footer>
        <div class="center">
            @if($contato->facebook || $contato->pinterest || $contato->rss || $contato->instagram)
            <div class="social">
                @if($contato->facebook)<a href="{{ $contato->facebook }}" class="facebook">facebook</a>@endif
                @if($contato->pinterest)<a href="{{ $contato->pinterest }}" class="pinterest">pinterest</a>@endif
                @if($contato->rss)<a href="{{ $contato->rss }}" class="rss">rss</a>@endif
                @if($contato->instagram)<a href="{{ $contato->instagram }}" class="instagram">instagram</a>@endif
            </div>
            @endif

            <p class="telefones">
                @foreach(explode(',', $contato->telefones) as $telefone)
                <span>{{ trim($telefone) }}</span>
                @endforeach
            </p>
            <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
        </div>
    </footer>

    <div class="copyright">
        <div class="center">
            <p>© {{ date('Y') }} {{ config('site.name') }} - Todos os direitos reservados.</p>
            <p><a href="http://trupe.net" target="_blank">Criação de sites</a>: <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a></p>
        </div>
    </div>
