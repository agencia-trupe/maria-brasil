    <header>
        <div class="center">
            <h1><a href="{{ route('home') }}">{{ config('site.name') }}</a></h1>
            <nav>
                @include('frontend.common.nav')
            </nav>
        </div>
    </header>
