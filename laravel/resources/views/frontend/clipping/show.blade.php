@extends('frontend.common.template')

@section('content')

    <div class="main clipping">
        <div class="center">
            <h2>Clipping</h2>

            <div class="clipping-show">
                <h3>{{ $clipping->titulo }}</h3>

                @foreach($clipping->imagens as $imagem)
                    <img src="{{ asset('assets/img/clipping/imagens/'.$imagem->imagem) }}" alt="">
                @endforeach

                <a href="{{ route('clipping') }}">Voltar</a>
            </div>
        </div>
    </div>

@endsection
