@extends('frontend.common.template')

@section('content')

    <div class="main clipping">
        <div class="center">
            <h2>Clipping</h2>

            <div class="clipping-list">
                @foreach($clipping as $clipping)
                <a href="{{ route('clipping', $clipping->slug) }}" class="clipping-thumb">
                    <img src="{{ asset('assets/img/clipping/thumbs/'.$clipping->imagem) }}" alt="">
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
