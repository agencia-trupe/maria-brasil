@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <h2>Contato</h2>

            <div class="contato-inner">
                <p class="telefones">
                    @foreach(explode(',', $contato->telefones) as $telefone)
                    <span>{{ trim($telefone) }}</span>
                    @endforeach
                </p>
                <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>

                <form action="" id="form-contato" method="POST">
                    <input type="text" name="nome" id="nome" placeholder="Nome" required>
                    <input type="email" name="email" id="email" placeholder="E-mail" required>
                    <input type="text" name="assunto" id="assunto" placeholder="Assunto">
                    <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required></textarea>
                    <div class="response-wrapper">
                        <div id="form-contato-response"></div>
                    </div>
                    <input type="submit" value="Enviar">
                </form>

                <div class="endereco">
                    {!! $contato->endereco !!}
                </div>
            </div>
        </div>

        <div class="googlemaps">
            {!! $contato->googlemaps !!}
        </div>
    </div>

@endsection
