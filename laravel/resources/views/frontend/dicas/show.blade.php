@extends('frontend.common.template')

@section('content')

    <div class="main dicas">
        <div class="center">
            <h2>Dicas</h2>

            <div class="dicas-show">
                <h3>{{ $dica->titulo }}</h3>

                <img src="{{ asset('assets/img/dicas/'.$dica->imagem) }}" alt="">

                <div class="texto">
                    {!! $dica->texto !!}
                    <a href="{{ route('dicas') }}">Voltar</a>
                </div>
            </div>
        </div>
    </div>

@endsection
