@extends('frontend.common.template')

@section('content')

    <div class="main dicas">
        <div class="center">
            <h2>Dicas</h2>

            <div class="dicas-list">
                @foreach($dicas as $dica)
                <a href="{{ route('dicas', $dica->slug) }}" class="dica-thumb">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/dicas/thumbs/'.$dica->imagem) }}" alt="">
                    </div>

                    <div class="texto">
                        <h3>{{ $dica->titulo }}</h3>
                        <p>{{ $dica->chamada }}</p>
                        <span>Veja a dica completa</span>
                    </div>

                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
