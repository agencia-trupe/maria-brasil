@extends('frontend.common.template')

@section('content')

    <div class="main projetos">
        <div class="center">
            <h2>Projetos</h2>

            <div class="projetos-categorias">
                @foreach($categorias as $categoria)
                <a href="{{ route('projetos.index', $categoria->slug) }}" @if($categoria_selecionada->slug == $categoria->slug) class="active" @endif>
                    {!! preg_replace('/,/', '<br>', trim($categoria->titulo)) !!}
                </a>
                @endforeach
            </div>

            <div class="projetos-list">
                @foreach($projetos as $projeto)
                <a href="{{ route('projetos.show', [$projeto->categoria->slug, $projeto->slug]) }}" class="projetos-thumb">
                    <img src="{{ asset('assets/img/projetos/thumbs/'.$projeto->imagem) }}" alt="">
                    <span>{{ $projeto->titulo }}</span>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
