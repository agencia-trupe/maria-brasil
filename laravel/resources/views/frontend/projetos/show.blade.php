@extends('frontend.common.template')

@section('content')

    <div class="main projetos">
        <div class="center">
            <h2>Projetos</h2>

            <div class="projetos-show">
                <img src="{{ asset('assets/img/projetos/'.$projeto->imagem) }}" alt="">

                <div class="texto">
                    <h3>{{ $projeto->titulo }}</h3>
                    {!! $projeto->texto !!}
                </div>

                @foreach($projeto->imagens as $imagem)
                    <img src="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" alt="">
                @endforeach

                <a href="{{ route('projetos.index', $projeto->categoria->slug) }}">Voltar</a>
            </div>
        </div>
    </div>

@endsection
