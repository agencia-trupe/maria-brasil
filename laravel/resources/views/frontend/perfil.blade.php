@extends('frontend.common.template')

@section('content')

    <div class="main perfil">
        <div class="center">
            <h2>Perfil</h2>

            <div class="texto">
                {!! $perfil->texto !!}
            </div>

            <img src="{{ asset('assets/img/perfil/'.$perfil->imagem) }}" alt="">
        </div>
    </div>

@endsection
