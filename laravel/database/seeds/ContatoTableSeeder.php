<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'telefones'  => '11 2659 8056, 11 2659 8057, 11 2659 8058',
            'email'      => 'contato@mariabrasilarquitetura.com.br',
            'endereco'   => '<p>Av. Brigadeiro Faria Lima, 2013</p><p>Conj. 5e - 7 - Pinheiros</p><p>São Paulo - SP - CEP: 01452-001</p>',
            'googlemaps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d117016.47131695956!2d-46.63300512050714!3d-23.576891879574223!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5772eee33c85%3A0x1f162bd68723b753!2sAv.+Brg.+Faria+Lima%2C+2013+-+Jardim+Paulistano%2C+S%C3%A3o+Paulo+-+SP%2C+01452-001!5e0!3m2!1spt-BR!2sbr!4v1452616579156" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
        ]);
    }
}
